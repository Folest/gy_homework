﻿using System;
using System.Collections.Generic;
using System.Text;
using BookEvidence.Domain.Books;
using BookEvidence.Domain.Users;
using BookEvidence.Service;

namespace BookEvidence
{
    class Program
    {
        private static LibraryService _libraryService;

        private static string EnumerableOfBooksToString(IEnumerable<BookRecord> books)
        {
            StringBuilder booksStringBuilder = new StringBuilder();
            foreach (var book in books)
            {
                booksStringBuilder.AppendLine(book.ToString());
            }

            return booksStringBuilder.ToString();
        }

        private static string EnumerableOfPersonRentalToString(IEnumerable<SimpleUserRecord> users)
        {
            StringBuilder personStringBuilder = new StringBuilder();
            foreach (var user in users)
            {
                personStringBuilder.AppendLine($"{user.BasicUserData.Name} {user.BasicUserData.Surname} is currently borrowing: {user.BorrowedLibraryElements.Count} books \n");
            }

            return personStringBuilder.ToString();
        }

        private static bool AddBook()
        {
            Console.WriteLine("Please enter book title:");
            var title = Console.ReadLine();
            Console.WriteLine("Please enter book author:");
            var author = Console.ReadLine();
            Console.WriteLine("Please enter book isbn:");
            var isbn = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(title) || string.IsNullOrWhiteSpace(author) ||
                string.IsNullOrWhiteSpace(isbn)) 
                return false;

            _libraryService.AddBook(title, author, isbn);
            return true;
        }

        private static void FindBook()
        {
            Console.WriteLine("Would you like to find book by (1.) title or (2.) ISBN? Press other key to cancel");
            var choice = Console.ReadKey().KeyChar;
            switch (choice)
            {
                case '1':
                    Console.WriteLine("Please enter the title");
                    var title = Console.ReadLine();
                    Console.WriteLine($"Books found: \n {EnumerableOfBooksToString(_libraryService.FindBookByTitle(title))}");
                    break;
                case '2':
                    Console.WriteLine("Please enter the ISBN");
                    var isbn = Console.ReadLine();
                    Console.WriteLine($"Books found: \n {EnumerableOfBooksToString(_libraryService.FindBookByIsbn(isbn))}");
                    break;
                default:
                    Console.WriteLine($"No books were found");
                    return;
            }
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        private static void RemoveBook()
        {
            Console.WriteLine("Would you like to remove book by (1.) title or (2.) ISBN? Press other key to cancel");
            var choice = Console.ReadKey().KeyChar;
            switch (choice)
            {
                case '1':
                    Console.WriteLine("Please enter the title");
                    var title = Console.ReadLine();
                    _libraryService.RemoveBookByTitle(title);
                    Console.WriteLine($"Books removed");
                    break;
                case '2':
                    Console.WriteLine("Please enter the ISBN");
                    var isbn = Console.ReadLine();
                    _libraryService.RemoveBookByIsbn(isbn);
                    Console.WriteLine($"Books removed");
                    break;
                default:
                    Console.WriteLine($"No books were removed");
                    break;
            }
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        private static void  BorrowABook()
        {
            Console.WriteLine("Would you like to borrow a book by (1.) title or (2.) ISBN? Press other key to cancel");
            var choice = Console.ReadKey().KeyChar;
            string title = null, isbn = null;
            switch (choice)
            {
                case '1':
                    Console.WriteLine("Please enter the title");
                    title = Console.ReadLine();
                    break;
                case '2':
                    Console.WriteLine("Please enter the ISBN");
                    isbn = Console.ReadLine();
                    break;
                default:
                    Console.WriteLine($"Canceled");
                    return;
            }

            Console.WriteLine("Please enter borrowing persons name");
            var name = Console.ReadLine();

            Console.WriteLine("Please enter borrowing persons surname");
            var surname = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(title) &&
                string.IsNullOrWhiteSpace(isbn))
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(surname))
            {
                bool rentalSucceded = false;
                if (choice == '1')
                    rentalSucceded = _libraryService.LendABookByTitle(
                        title, name,surname, DateTime.Now);
                else if (choice == '2')
                {
                    rentalSucceded = _libraryService.LendABookByIsbn(
                        isbn, name, surname, DateTime.Now);
                }

                if (rentalSucceded)
                {
                    Console.WriteLine("You've succesfully borrowed a book");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    return;
                }
                else
                {
                    Console.WriteLine("There is no such a book");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    return;
                }
            }
            Console.WriteLine("Name or surname was empty");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        private static void ShowPeopleBorrowingBooks()
        {
            Console.WriteLine(EnumerableOfPersonRentalToString(_libraryService.GetPeopleBorrowingBooks()));
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        private static void GetBooksNotBorrowedForNumberOfWeeks()
        {
            Console.WriteLine("Please specify number of weeks from last rental");
            var weekNumber = Console.ReadLine();
            if (int.TryParse(weekNumber,out var weeks))
            {
                bool anySatisfiesCondition = false;
                foreach (var book in
                    _libraryService.BooksNotBorrowedForNumberOfWeeks(weeks))
                {
                    Console.WriteLine($"Book titled {book.Title} was not lent for" +
                                      $"{(book.LastBorrowDate - DateTime.Today).Value.Days / 7} weeks");
                    anySatisfiesCondition = true;
                }

                if (!anySatisfiesCondition)
                {
                    Console.WriteLine("No such book was found");
                }
            }
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        private static void Greet()
        {
            Console.WriteLine("Hello User!");
        }

        public static void DisplayMenu()
        {
            Console.WriteLine("Please enter one of the following options");
            Console.WriteLine("1. Add a book to library");
            Console.WriteLine("2. Delete a book from library");
            Console.WriteLine("3. Find a book");
            Console.WriteLine("4. Show books not borrowed for a number of weeks");
            Console.WriteLine("5. Borrow a book");
            Console.WriteLine("6. Display people with books lent");
            Console.WriteLine("q. Quit");
        }

        enum Options
        {
            AddToLibrary = 1,
            RemoveFromLibrary,
            FindABook, 
            ShowBooksNotBorredForNumberOfWeeks, 
            BorrowABook, 
            DisplayPeopleWithBooksBorrowed,
            Serialize
        }

        //first positional argument is Book repository persistence file path and second is the same for user repository
        static void Main(string[] args) 
        {
            if (args.Length == 2)
            {
                _libraryService = new LibraryService(args[0], args[1]);
            }
            else
            {
                _libraryService = new LibraryService();
            }
            Greet();

            string userInput;
            do
            {
                Console.Clear();

                DisplayMenu();
                
                userInput = new string (Console.ReadKey().KeyChar,1);
                if (Options.TryParse(userInput, true, out Options option))
                {
                    Console.Clear();
                    switch (option)
                    {
                        case Options.AddToLibrary:
                            AddBook();
                            break;
                        case Options.RemoveFromLibrary:
                            RemoveBook();
                            break;
                        case Options.FindABook:
                            FindBook();
                            break;
                        case Options.ShowBooksNotBorredForNumberOfWeeks:
                            GetBooksNotBorrowedForNumberOfWeeks();
                            break;
                        case Options.BorrowABook:
                            BorrowABook();
                            break;
                        case Options.DisplayPeopleWithBooksBorrowed:
                            ShowPeopleBorrowingBooks();
                            break;
                        default:
                            continue;
                    }
                }

            } while (userInput != "q");
        }
    }
}