﻿using System;
using System.Text;
using BookEvidence.Domain.Users;

namespace BookEvidence.Domain.Books
{
    public class BookRecord : LibraryElementRecordBase
    {
        public BookRecord(){}

        public BookRecord(string title, string author, string isbn)
        {
            Title = title;
            Author = author;
            Isbn = isbn;
        }

        public BookRecord(string title, string author, string isbn,
            BasicUserData lastBorrowingUser
            ,DateTime? lastBorrowDate = null)
        {
            Title = title;
            Author = author;
            Isbn = isbn;
            LastBorrowDate = lastBorrowDate;
            LastBorrowingPerson = lastBorrowingUser;
        }

        public override string ToString()
        {
            StringBuilder bookToString = new StringBuilder();
            bookToString.Append($"{nameof(Title)}: {Title}, " +
                                $"{nameof(Author)}: {Author}, " +
                                $"{nameof(Isbn)}: {Isbn} ");
            bookToString.Append(LastBorrowDate != null ? 
                $" Last borrowing person {LastBorrowingPerson} borrowed on {LastBorrowDate.ToString()}" : "Not borrowed yet");

            return bookToString.ToString();
        }

    }
}
