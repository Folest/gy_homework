﻿using System;
using BookEvidence.Domain.Users;
using BookEvidence.Mappers;

namespace BookEvidence.Domain.Books
{
    public abstract class LibraryElementRecordBase : MappingEntity
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Isbn { get; set; }
        public DateTime? LastBorrowDate { get; set; }
        public BasicUserData LastBorrowingPerson { get; set; }
    }
}