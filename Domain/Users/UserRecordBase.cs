﻿using System.Collections.Generic;
using BookEvidence.Domain.Books;
using BookEvidence.Mappers;

namespace BookEvidence.Domain.Users
{
    public abstract class UserRecordBase : MappingEntity
    {
        public BasicUserData BasicUserData { get; set; }

        public int Id { get; set; }

        public List<LibraryElementRecordBase> BorrowedLibraryElements { get; set; }
    }
}
