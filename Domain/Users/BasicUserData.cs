﻿namespace BookEvidence.Domain.Users
{
    public struct BasicUserData
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public BasicUserData(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }
    }
}
