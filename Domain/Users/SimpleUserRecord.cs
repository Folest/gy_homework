﻿using System.Collections.Generic;
using BookEvidence.Domain.Books;

namespace BookEvidence.Domain.Users
{
    public class SimpleUserRecord : UserRecordBase
    {
        public SimpleUserRecord(){}
        
        public SimpleUserRecord(string name, string surname)
        {
            BasicUserData = new BasicUserData(name, surname);

            Id = 1;
            BorrowedLibraryElements = new List<LibraryElementRecordBase>();
        }
        public override string ToString()
        {
            return $"Person named: {BasicUserData}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;
            var other = (SimpleUserRecord) obj;
            return Equals(other);

        }

        private bool Equals(SimpleUserRecord other)
        {
            return string.Equals(BasicUserData.Name, other.BasicUserData.Name) && string.Equals(BasicUserData.Name, other.BasicUserData.Surname) && Id == other.Id;
        }

    }
}
