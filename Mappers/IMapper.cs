﻿using System.Collections.Generic;

namespace BookEvidence.Mappers
{
    interface IMapper<TMainElement>
    where TMainElement: MappingEntity
    {
        void Insert(TMainElement inserted);
        TMainElement Find(int id);
        IEnumerable<TMainElement> GetAll();
        void Update(TMainElement updated);
        void Delete(int deletedId);
    }
}
