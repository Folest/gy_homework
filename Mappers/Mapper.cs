﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BookEvidence.Mappers
{
    class Mapper<TMainEntry, TSecondaryEntry> : IMapper<TMainEntry>
    where TMainEntry: MappingEntity
    {
        private string _targetPath;

        private readonly XmlSerializer _serializer;
        private readonly XmlSerializerNamespaces _namespaces;

        private XDocument _document;

        public Mapper(string path)
        {
            _targetPath = path;
            _serializer = new XmlSerializer(typeof(TMainEntry), new[] {typeof(TSecondaryEntry)});
            _namespaces = new XmlSerializerNamespaces();
            _namespaces.Add("", "");

            if (File.Exists(_targetPath))
            {
                _document = XDocument.Load(_targetPath);
            }
            else
            {
                _document = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));
                _document.Add(new XElement("Root"));
            }
        }

        public void Insert(TMainEntry inserted)
        {
            var insertedXml = ToXElement(inserted);
            _document.Root.Add(insertedXml);
            _document.Save(_targetPath);
        }

        public TMainEntry Find(int id)
        {
            return FromXml(_document.Root?.Elements(typeof(TMainEntry).Name)
                .FirstOrDefault(user => (string) user.Element("Id") == id.ToString()));
        }

        public void Delete(int deletedId)
        {
            _document.Root.Elements(typeof(TMainEntry).Name).Where(entry => (string) entry.Element("Id") == deletedId.ToString())
                .Remove();
            _document.Save(_targetPath);
        }

        public void Update(TMainEntry updated)
        {
            Delete(updated.Id);
            Insert(updated);
        }

        private XElement ToXElement(object transformed)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamTextWriter = new StreamWriter(memoryStream))
                {
                    _serializer.Serialize(streamTextWriter, transformed, _namespaces);
                }

                return XElement.Parse(Encoding.UTF8.GetString(memoryStream.ToArray()));
            }
        }

        private TMainEntry FromXml(XElement userElement)
        {
            return (TMainEntry) _serializer.Deserialize(userElement.CreateReader());
        }

        public IEnumerable<TMainEntry> GetAll()
        {
            foreach (var element in _document.Root.Elements(typeof(TMainEntry).Name))
            {
                yield return FromXml(element);
            }
        }
    }
}