﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookEvidence.Domain.Books;
using BookEvidence.Domain.Users;
using BookEvidence.Repositories;

namespace BookEvidence.Service
{
    public class LibraryService
    {
        private readonly IRepository<BookRecord> _bookRepository = null;
        private readonly IRepository<SimpleUserRecord> _userRepository = null;

        public LibraryService(string bookPersistenceFilePath = "targetBook.xml",string userPersistenceFilePath = "targetUsers.xml")
        {
            _userRepository = new UserRepository<SimpleUserRecord,BookRecord>(userPersistenceFilePath);
            _bookRepository = new BookRepostiory<BookRecord>(bookPersistenceFilePath);
        }
       
        public void AddBook(string title, string author, string isbn)
        {
            _bookRepository.Add(new BookRecord(title, author, isbn));
        }

        public IEnumerable<BookRecord> FindBookByTitle(string title)
        {
            return _bookRepository.GetAll(x => x.Title == title);
        }

        public IEnumerable<BookRecord> FindBookByIsbn(string isbn)
        {
            return _bookRepository.GetAll(x => x.Isbn == isbn);
        }

        public void RemoveBookByTitle(string title)
        {
            _bookRepository.DeleteAll(x => x.Title == title);
        }

        public void RemoveBookByIsbn(string isbn)
        {
            _bookRepository.DeleteAll(x => x.Isbn == isbn);
        }

        //Not safe when there are Name and Surname duplicates
        public bool LendABookByTitle(string title, string lenderName, string lenderSurname, DateTime lendDate)
        {
            var books = _bookRepository.GetAll(x => x.Title == title);
            if (!books.Any()) return false;

            var lendedBook = books.First();

            lendedBook.LastBorrowDate = lendDate;
            if (_userRepository.GetAll().Any(x => x.BasicUserData.Name == lenderName && x.BasicUserData.Surname == lenderSurname))
            {
                var lender = _userRepository
                    .GetAll(user => user.BasicUserData.Name == lenderName && user.BasicUserData.Surname == lenderSurname)
                    .First();

                lender.BorrowedLibraryElements.Add(lendedBook);
                lendedBook.LastBorrowingPerson = lender.BasicUserData;

                _userRepository.Update(lender);
                _bookRepository.Update(lendedBook);
            }
            else
            {
                var newUser = new SimpleUserRecord(lenderName,lenderSurname);
                newUser.BorrowedLibraryElements.Add(lendedBook);
                _userRepository.Add(newUser);
                lendedBook.LastBorrowingPerson = newUser.BasicUserData;

                _bookRepository.Update(lendedBook);
            }

            return true;
        }

        public bool LendABookByIsbn(string isbn, string lenderName, string lenderSurname, DateTime lendDate)
        {
            var books = _bookRepository.GetAll(x => x.Isbn == isbn);
            if (!books.Any())
                return false;

            var lendedBook = books.First();

            lendedBook.LastBorrowDate = lendDate;
            if (_userRepository.GetAll().Any(x => x.BasicUserData.Name == lenderName && x.BasicUserData.Surname == lenderSurname))
            {
                var lender = _userRepository
                    .GetAll(user => user.BasicUserData.Name == lenderName && user.BasicUserData.Surname == lenderSurname)
                    .First();

                lender.BorrowedLibraryElements.Add(lendedBook);
                lendedBook.LastBorrowingPerson = lender.BasicUserData;

                _userRepository.Update(lender);
                _bookRepository.Update(lendedBook);
            }
            else
            {
                var newUser = new SimpleUserRecord(lenderName,lenderSurname);
                newUser.BorrowedLibraryElements.Add(lendedBook);
                _userRepository.Add(newUser);
                lendedBook.LastBorrowingPerson = newUser.BasicUserData;
                _bookRepository.Update(lendedBook);
            }

            return true;
        }

        public IEnumerable<BookRecord> BooksNotBorrowedForNumberOfWeeks(int weeks)
        {
            return _bookRepository.GetAll(x => x.LastBorrowDate - DateTime.Now >= new TimeSpan(0, 0, 7 * weeks));
        }

        public IEnumerable<SimpleUserRecord> GetPeopleBorrowingBooks()
        {
            return _userRepository.GetAll(user => user.BorrowedLibraryElements.Any());
        }
    }
}