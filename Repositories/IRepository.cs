﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BookEvidence.Mappers;

namespace BookEvidence.Repositories
{
    interface IRepository<T>
    where T : MappingEntity
    {
        T GetById(int id);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate);

        void Add(T item);
        void DeleteAll(Predicate<T> predicate);
        void Update(T item);
    }
}
