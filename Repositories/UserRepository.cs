﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BookEvidence.Domain.Books;
using BookEvidence.Domain.Users;
using BookEvidence.Mappers;

namespace BookEvidence.Repositories
{
    class UserRepository<TUserRecord,TLibraryElementRecord>: IRepository<TUserRecord>
    where TUserRecord : UserRecordBase
    where TLibraryElementRecord : LibraryElementRecordBase
    {
        private Mapper<TUserRecord,TLibraryElementRecord> _mapper;

        private List<TUserRecord> _users;

        public static int CurrentId { get; private set; } = 0;

        public UserRepository(string targetFilePath)
        {
            _mapper = new Mapper<TUserRecord, TLibraryElementRecord>(targetFilePath);
            _users = _mapper.GetAll().ToList();
        }

        public TUserRecord GetById(int id)
        {
            return _users.Find(x => x.Id == id);
        }

        public IEnumerable<TUserRecord> GetAll()
        {
            return _users.AsEnumerable();
        }

        public IEnumerable<TUserRecord> GetAll(Expression<Func<TUserRecord, bool>> predicate)
        {
            if (predicate != null) return _users.AsQueryable().Where(predicate);
            throw new ArgumentNullException();
        }

        public void Add(TUserRecord item)
        {
            item.Id = CurrentId++;
            _users.Add(item);
            _mapper.Insert(item);
        }

        public void DeleteAll(Predicate<TUserRecord> predicate)
        {
            if (predicate != null)
            {
                foreach (var simpleUserRecord in _users.Where( x => predicate(x)))
                {
                    _mapper.Delete(simpleUserRecord.Id);
                }
            }
            _users.Clear();
        }

        public void Update(TUserRecord item)
        {
            _mapper.Update(item);
        }
    }
}
