﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BookEvidence.Domain.Books;
using BookEvidence.Domain.Users;
using BookEvidence.Mappers;

namespace BookEvidence.Repositories
{
    class BookRepostiory<TLibraryElementRecord>: IRepository<TLibraryElementRecord>
        where TLibraryElementRecord : LibraryElementRecordBase
    {
        private Mapper<TLibraryElementRecord,BasicUserData> _mapper;

        private List<TLibraryElementRecord> _books;

        public static int CurrentId { get; private set; } = 0;


        public BookRepostiory(string targetFilePath)
        {
            _mapper = new Mapper<TLibraryElementRecord, BasicUserData>(targetFilePath);
            _books = _mapper.GetAll().ToList();
        }

        public TLibraryElementRecord GetById(int id)
        {
            return _books.Find(x => x.Id == id);
        }

        public IEnumerable<TLibraryElementRecord> GetAll()
        {
            return _books.AsEnumerable();
        }

        public IEnumerable<TLibraryElementRecord> GetAll(Expression<Func<TLibraryElementRecord, bool>> predicate)
        {
            if (predicate != null) return _books.AsQueryable().Where(predicate);
            throw new ArgumentNullException();
        }

        public void Add(TLibraryElementRecord item)
        {
            item.Id = CurrentId++;
            _books.Add(item);
            _mapper.Insert(item);
        }

        public void DeleteAll(Predicate<TLibraryElementRecord> predicate)
        {
            if (predicate != null)
            {
                foreach (var simpleUserRecord in _books.Where(x => predicate(x)))
                {
                    _mapper.Delete(simpleUserRecord.Id);
                }

                _books.RemoveAll(predicate);
            }
        }

        public void Update(TLibraryElementRecord item)
        {
            _mapper.Update(item);
        }
    }
}
